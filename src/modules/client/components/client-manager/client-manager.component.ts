import {Component} from '@angular/core';
import {Client} from '../../models/client';
import {ClientProvider} from '../../provider/client.provider';
import {DropdownItem} from '../../../shared/models/dropdown-item';

@Component({
  selector: 'client-manager',
  templateUrl: './client-manager.component.html',
  styleUrls: ['./client-manager.component.scss']
})
export class ClientManagerComponent {
  public client: Client | undefined;

  public constructor(private clientProvider: ClientProvider) {
    this.clientProvider.clientObservable.subscribe((client: Client) => {
      this.client = client;
    });
  }

  public get dropdownItems(): Array<DropdownItem> {
    return [
      new DropdownItem(this.client.email, null, true),
      new DropdownItem('Profile', ['#']),
      new DropdownItem('Logout', ['#']),
    ];
  }
}
