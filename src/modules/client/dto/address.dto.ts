export class AddressDTO {
  public constructor(
    public id?: string,
    public name?: string,
    public surname?: string,
    public street?: string,
    public buildingNumber?: string,
    public city?: string,
    public zip?: string) {
  }
}
