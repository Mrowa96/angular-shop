import {Injectable} from '@angular/core';
import {AddressDTO} from '../dto/address.dto';
import {Address} from '../models/address';
import {v4 as uuid} from 'uuid';

@Injectable()
export class AddressFactory {
  public create(addressDTO: AddressDTO): Address {
    return new Address(
      uuid(),
      addressDTO.name,
      addressDTO.surname,
      addressDTO.street,
      addressDTO.buildingNumber,
      addressDTO.city,
      addressDTO.zip
    );
  }
}
