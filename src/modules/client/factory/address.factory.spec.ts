import {AddressDTO} from '../dto/address.dto';
import {AddressFactory} from './address.factory';

describe('AddressFactory', () => {
  let factory: AddressFactory;

  beforeEach(() => {
    factory = new AddressFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Address object', () => {
    const dto = new AddressDTO();

    dto.name = 'Test';
    dto.surname = 'Test';
    dto.street = 'Test street';
    dto.buildingNumber = '1/17';
    dto.city = 'Test city';
    dto.zip = '20-222';

    const address = factory.create(dto);

    expect(address.name).toEqual(dto.name);
    expect(address.surname).toEqual(dto.surname);
    expect(address.street).toEqual(dto.street);
    expect(address.buildingNumber).toEqual(dto.buildingNumber);
    expect(address.city).toEqual(dto.city);
    expect(address.zip).toEqual(dto.zip);
  });
});
