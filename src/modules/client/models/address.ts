export class Address {
  public constructor(
    private _id: string,
    private _name: string,
    private _surname: string,
    private _street: string,
    private _buildingNumber: string,
    private _city: string,
    private _zip: string) {
  }

  public get id(): string {
    return this._id;
  }

  public get name(): string {
    return this._name;
  }

  public get surname(): string {
    return this._surname;
  }

  public get street(): string {
    return this._street;
  }

  public get buildingNumber(): string {
    return this._buildingNumber;
  }

  public get city(): string {
    return this._city;
  }

  public get zip(): string {
    return this._zip;
  }

  public get fullName(): string {
    return `${this.name} ${this.surname}`;
  }

  public toString(): string {
    return this.fullName;
  }
}
