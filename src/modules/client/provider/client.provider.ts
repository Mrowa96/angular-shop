import {Client} from '../models/client';
import {Address} from '../models/address';
import {Injectable} from '@angular/core';
import {v4 as uuid} from 'uuid';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class ClientProvider {
  private readonly _client: Client;
  private clientSubject: BehaviorSubject<Client>;
  private readonly _clientObservable: Observable<Client>;

  public constructor() {
    this._client = this.createNewClient();
    this.clientSubject = new BehaviorSubject(this._client);
    this._clientObservable = this.clientSubject.asObservable();
  }

  private createNewClient(): Client {
    return new Client(uuid(), 'test@test.pl', new Address(
      uuid(), 'Test', 'Test', 'Test street', '23' , 'Test city', '01-234'
    ));
  }

  public get client(): Client {
    return this._client;
  }

  public get clientObservable(): Observable<Client> {
    return this._clientObservable;
  }
}
