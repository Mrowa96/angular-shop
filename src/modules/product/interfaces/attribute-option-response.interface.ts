export interface AttributeOptionResponseInterface {
  id: number;
  name: string;
}
