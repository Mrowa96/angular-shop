export interface PhotoResponseInterface {
  id: number;
  url: string;
  main: boolean;
}
