import {Attribute} from '../models/attribute';
import {AttributeOption} from '../models/attribute-option';

export interface AttributeWithCheckStatusInterface {
  attribute: Attribute;
  options: Array<{
    option: AttributeOption,
    checked: boolean
  }>;
}
