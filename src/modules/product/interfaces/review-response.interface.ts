export interface ReviewResponseInterface {
  id: number;
  rating: number;
  authorName: string;
  date: string;
  content: string;
}
