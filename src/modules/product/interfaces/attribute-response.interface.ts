import {AttributeOptionResponseInterface} from './attribute-option-response.interface';

export interface AttributeResponseInterface {
  id: number;
  name: string;
  filterable: boolean;
  options: Array<AttributeOptionResponseInterface>;
}
