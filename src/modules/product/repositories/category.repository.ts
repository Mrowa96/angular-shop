import {Observable, Observer} from 'rxjs';
import {Injectable} from '@angular/core';
import {CategoryClient} from '../client/category.client';
import {CategoryFactory} from '../factory/category.factory';
import {Category} from '../models/category';
import {CategoryResponseInterface} from '../interfaces/category-response.interface';

@Injectable()
export class CategoryRepository {
  public constructor(private client: CategoryClient, private factory: CategoryFactory) {
  }

  public getOne(id: number): Observable<Category | undefined> {
    return Observable.create((observer: Observer<Category>) => {
      this.client.getOne(id).subscribe((data: CategoryResponseInterface) => {
        observer.next(this.factory.create(data));
      }, () => {
        observer.next(undefined);
      });
    });
  }

  public getAll(): Observable<Category[]> {
    return Observable.create((observer: Observer<Category[]>) => {
      this.client.getAll().subscribe((data: CategoryResponseInterface[]) => {
        observer.next(
          this.factory.createCollection(data)
        );
      });
    });
  }
}
