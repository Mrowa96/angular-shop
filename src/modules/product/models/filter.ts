export class Filter {
  public constructor(private _attributeId: number, private _attributeOptionId: number) {
  }

  public get attributeId(): number {
    return this._attributeId;
  }

  public get attributeOptionId(): number {
    return this._attributeOptionId;
  }
}
