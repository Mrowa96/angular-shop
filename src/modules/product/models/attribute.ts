import {AttributeOption} from './attribute-option';

export class Attribute {
  public constructor(
    private _id: number,
    private _name: string,
    private _filterable: boolean,
    private _options: Array<AttributeOption>) {
  }

  public get id(): number {
    return this._id;
  }

  public get name(): string {
    return this._name;
  }

  public get filterable(): boolean {
    return this._filterable;
  }

  public get options(): Array<AttributeOption> {
    return this._options;
  }
}
