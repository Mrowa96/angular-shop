import {Attribute} from './attribute';

export class Category {
  public constructor(private _id: number, private _name: string, private _attributes: Array<Attribute> = []) {
  }

  public get id(): number {
    return this._id;
  }

  public get name(): string {
    return this._name;
  }

  public get attributes(): Array<Attribute> {
    return this._attributes;
  }

  public toString(): string {
    return this.name;
  }
}
