import {ProductModuleConfigInterface} from '../interfaces/product-module-config.interface';
import {InjectionToken} from '@angular/core';

export const PRODUCT_MODULE_GLOBAL_CONFIG =
  new InjectionToken<ProductModuleConfigInterface>('product-module-global-config');
