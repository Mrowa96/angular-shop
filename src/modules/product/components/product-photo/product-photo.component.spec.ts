import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductPhotoComponent} from './product-photo.component';
import {Product} from '../../models/product';
import {Category} from '../../models/category';
import {Photo} from '../../models/photo';

describe('ProductPhotoComponent', () => {
  let component: ProductPhotoComponent;
  let fixture: ComponentFixture<ProductPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductPhotoComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPhotoComponent);
    component = fixture.componentInstance;

    component.product = new Product(1, 'Test', 100, new Category(1, 'Test'), [
      new Photo(1, 'https://vignette.wikia.nocookie.net/arianagrande/images/7/70/Example.png/revision/latest?cb=20160301231046')
    ]);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
