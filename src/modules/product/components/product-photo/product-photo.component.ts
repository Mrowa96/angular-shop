import {Component, Input} from '@angular/core';
import {Product} from '../../models/product';

@Component({
  selector: 'product-photo',
  templateUrl: './product-photo.component.html',
  styleUrls: ['./product-photo.component.scss']
})
export class ProductPhotoComponent {
  @Input()
  public product: Product;
}
