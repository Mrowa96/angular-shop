import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductOnListComponent} from './product-on-list.component';
import {Product} from '../../models/product';
import {Category} from '../../models/category';
import {RouterTestingModule} from '@angular/router/testing';
import {BuyButtonComponent} from '../buy-button/buy-button.component';
import {PRODUCT_MODULE_GLOBAL_CONFIG} from '../../consts/product-module-global-config.const';
import {OrderItemFactory} from '../../../checkout/factory/order-item.factory';
import {OrderService} from '../../../checkout/services/order.service';
import {ProductPhotoComponent} from '../product-photo/product-photo.component';
import {OrderProvider} from '../../../checkout/provider/order.provider';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {Observable, Observer} from 'rxjs';
import {Order} from '../../../checkout/models/order';

describe('ProductOnListComponent', () => {
  const product = new Product(1, 'test', 100, new Category(1, 'test'));

  let component: ProductOnListComponent;
  let fixture: ComponentFixture<ProductOnListComponent>;
  let native;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductOnListComponent,
        BuyButtonComponent,
        ProductPhotoComponent
      ],
      imports: [
        FontAwesomeModule,
        RouterTestingModule
      ],
      providers: [
        OrderItemFactory,
        OrderService,
        {
          provide: OrderProvider,
          useValue: {
            orderObservable: Observable.create((observer: Observer<Order>) => {
              observer.next(undefined);
            })
          }
        },
        {
          provide: PRODUCT_MODULE_GLOBAL_CONFIG,
          useValue: {
            checkoutUrl: '/checkout'
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductOnListComponent);
    component = fixture.componentInstance;
    component.product = product;

    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display product name',  () => {
    expect(native.querySelector('.product-name').textContent).toEqual(product.name);
  });
});
