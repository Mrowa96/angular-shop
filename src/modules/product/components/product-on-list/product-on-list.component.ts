import {Component, Input} from '@angular/core';
import {Product} from '../../models/product';

@Component({
  selector: 'product-on-list',
  templateUrl: './product-on-list.component.html',
  styleUrls: ['./product-on-list.component.scss']
})
export class ProductOnListComponent {
  @Input()
  public product: Product;
}
