import {Component, Input} from '@angular/core';
import {Product} from '../../models/product';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent {
  @Input()
  public productCollection: Array<Product> | undefined;

  @Input()
  public noResultsMessage: string;

  public hasNoItems(): boolean {
    return this.productCollection && !this.productCollection.length;
  }

  public hasItems(): boolean {
    return !!this.productCollection && !!this.productCollection.length;
  }
}
