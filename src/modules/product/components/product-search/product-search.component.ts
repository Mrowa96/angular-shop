import {Component, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss']
})
export class ProductSearchComponent {
  @ViewChild('phraseInput')
  public phraseInputRef;

  public phrase: string;

  public constructor(private router: Router) {
  }

  public onSubmit(): void {
    this.router.navigate(['/search', this.phrase]).then(() => {
      this.phraseInputRef.nativeElement.blur();
      this.clearPhrase();
    });
  }

  private clearPhrase(): void {
    this.phrase = undefined;
  }
}
