import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductReviewsComponent} from './product-reviews.component';
import {Product} from '../../models/product';
import {Category} from '../../models/category';
import {Review} from '../../models/review';
import {ReviewRepository} from '../../repositories/review.repository';
import {FormsModule} from '@angular/forms';
import {NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import {ReviewFactory} from '../../factory/review.factory';

describe('ProductReviewsComponent', () => {
  let component: ProductReviewsComponent;
  let fixture: ComponentFixture<ProductReviewsComponent>;
  let native;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductReviewsComponent
      ],
      imports: [
        FormsModule,
        NgbRatingModule.forRoot()
      ],
      providers: [
        ReviewRepository,
        ReviewFactory
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductReviewsComponent);

    component = fixture.componentInstance;
    component.product = new Product(1, 'test', 100, new Category(1, 'test'), [], [
      new Review(1, 4, 'Test', new Date(), 'Test content')
    ]);

    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display one review',  () => {
    expect(native.querySelectorAll('.reviews .review').length).toEqual(1);
  });

  it('should display new review form',  () => {
    expect(native.querySelector('.new-review form')).toBeTruthy();
  });
});
