import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../models/product';
import {ReviewDTO} from '../../dto/review.dto';
import {ReviewRepository} from '../../repositories/review.repository';
import {FormStatus} from '../../enums/form-status.enum';

@Component({
  selector: 'product-reviews',
  templateUrl: './product-reviews.component.html',
  styleUrls: ['./product-reviews.component.scss']
})
export class ProductReviewsComponent implements OnInit {
  @Input()
  public product: Product;

  public review: ReviewDTO;
  public status: FormStatus = FormStatus.Initial;
  public formStatus = FormStatus;

  private readonly DEFAULT_RATING: number = 3;

  public constructor(private reviewRepository: ReviewRepository) {
  }

  public ngOnInit(): void {
    this.newReview();
  }

  public addReview(): void {
    this.status = FormStatus.Sending;

    this.reviewRepository.add(this.review).subscribe((result: boolean) => {
      if (result) {
        this.status = FormStatus.Success;
      } else {
        this.status = FormStatus.Fail;
      }
    });
  }

  private newReview(): void {
    this.review = new ReviewDTO();
    this.review.productId = this.product.id;
    this.review.rating = this.DEFAULT_RATING;
  }
}
