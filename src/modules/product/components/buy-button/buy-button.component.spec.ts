import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BuyButtonComponent} from './buy-button.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderItemFactory} from '../../../checkout/factory/order-item.factory';
import {OrderService} from '../../../checkout/services/order.service';
import {PRODUCT_MODULE_GLOBAL_CONFIG} from '../../consts/product-module-global-config.const';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

describe('BuyButtonComponent', () => {
  let component: BuyButtonComponent;
  let fixture: ComponentFixture<BuyButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BuyButtonComponent
      ],
      imports: [
        FontAwesomeModule,
        RouterTestingModule
      ],
      providers: [
        OrderItemFactory,
        {
          provide: OrderService,
          useValue: {
            addOrderItem: () => {}
          }
        },
        {
          provide: PRODUCT_MODULE_GLOBAL_CONFIG,
          useValue: {
            checkoutUrl: '/checkout'
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
