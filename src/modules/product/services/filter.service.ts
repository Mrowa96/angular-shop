import {Injectable} from '@angular/core';
import {ProductResponseInterface} from '../interfaces/product-response.interface';
import {Filter} from '../models/filter';
import {AttributeOptionResponseInterface} from '../interfaces/attribute-option-response.interface';

@Injectable()
export class FilterService {
  public filterResponse(data: ProductResponseInterface[], filters: Filter[]): ProductResponseInterface[] {
    const filtersMap = this.groupFilterOptions(filters);

    return data.filter((product: ProductResponseInterface): boolean => {
      for (const attributeId in filtersMap) {
        if (filtersMap.hasOwnProperty(attributeId)) {
          if (!this.hasAttribute(+attributeId, product)) {
            return false;
          }

          if (!this.hasAnyAttributeOption(+attributeId, filtersMap[attributeId], product)) {
            return false;
          }
        }
      }

      return true;
    });
  }

  private hasAttribute(attributeId: number, product: ProductResponseInterface): boolean {
    for (const attribute of product.attributes) {
      if (attributeId === attribute.id) {
        return true;
      }
    }

    return false;
  }

  private hasAnyAttributeOption(attributeId: number, optionIds: Array<number>, product: ProductResponseInterface): boolean {
    for (const attribute of product.attributes) {
      if (attributeId === attribute.id) {
        for (const optionId of optionIds) {
          const foundOption = attribute.options.find((option: AttributeOptionResponseInterface) => {
            return option.id === optionId;
          });

          if (foundOption) {
            return true;
          }
        }
      }
    }

    return false;
  }

  private groupFilterOptions(filters: Filter[]) {
    const mappedFilters = {};

    filters.map((filter: Filter) => {
      if (!mappedFilters[filter.attributeId]) {
        mappedFilters[filter.attributeId] = [];
      }

      mappedFilters[filter.attributeId].push(filter.attributeOptionId);
    });

    return mappedFilters;
  }
}
