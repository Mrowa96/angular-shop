import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Product} from '../../models/product';
import {SwiperConfigInterface} from 'ngx-swiper-wrapper';
import {ProductRepository} from '../../repositories/product.repository';
import {Breadcrumb} from '../../../shared/models/breadcrumb';

@Component({
  templateUrl: './product-detail-page.component.html',
  styleUrls: ['./product-detail-page.component.scss']
})
export class ProductDetailPageComponent implements OnInit {
  public product: Product | undefined;
  public contentLoading: boolean = false;
  public sliderConfig: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 1,
    centeredSlides: true,
    navigation: {
      prevEl: '.button-prev',
      nextEl: '.button-next'
    },
    simulateTouch: false
  };

  public constructor(private productRepository: ProductRepository, private route: ActivatedRoute) {
  }

  public ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.contentLoading = true;

      this.productRepository.getOne(+params.get('productId')).subscribe((product: Product | undefined) => {
        this.product = product;
        this.contentLoading = false;
      });
    });
  }

  public get breadcrumbsItems(): Array<Breadcrumb> {
    return [
      new Breadcrumb('Home', ['/']),
      new Breadcrumb(this.product.category.name, ['/product-list', this.product.category.id]),
      new Breadcrumb(this.product.name),
    ];
  }
}
