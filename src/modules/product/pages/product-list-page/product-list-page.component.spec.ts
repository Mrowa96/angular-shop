import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductClient} from '../../client/product.client';
import {ProductFactory} from '../../factory/product.factory';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from '../../../shared/shared.module';
import {PhotoFactory} from '../../factory/photo.factory';
import {CategoryFactory} from '../../factory/category.factory';
import {ProductRepository} from '../../repositories/product.repository';
import {ProductListPageComponent} from './product-list-page.component';
import {ProductListComponent} from '../../components/product-list/product-list.component';
import {ProductDetailPageComponent} from '../product-detail-page/product-detail-page.component';
import {SearchListPageComponent} from '../search-list-page/search-list-page.component';
import {NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import {ProductReviewsComponent} from '../../components/product-reviews/product-reviews.component';
import {CategoryNavigationComponent} from '../../components/category-navigation/category-navigation.component';
import {ProductPhotoComponent} from '../../components/product-photo/product-photo.component';
import {ProductListFilterManagerComponent} from '../../components/product-list-filter-manager/product-list-filter-manager.component';
import {ProductOnListComponent} from '../../components/product-on-list/product-on-list.component';
import {BuyButtonComponent} from '../../components/buy-button/buy-button.component';
import {SwiperModule} from 'ngx-swiper-wrapper';
import {MatProgressSpinnerModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {AttributeOptionFactory} from '../../factory/attribute-option.factory';
import {ReviewFactory} from '../../factory/review.factory';
import {OrderService} from '../../../checkout/services/order.service';
import {FilterFactory} from '../../factory/filter.factory';
import {PRODUCT_MODULE_GLOBAL_CONFIG} from '../../consts/product-module-global-config.const';
import {AttributeFactory} from '../../factory/attribute.factory';
import {OrderItemFactory} from '../../../checkout/factory/order-item.factory';
import {ReviewRepository} from '../../repositories/review.repository';
import {CategoryRepository} from '../../repositories/category.repository';
import {CategoryClient} from '../../client/category.client';
import {OrderProvider} from '../../../checkout/provider/order.provider';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FilterService} from '../../services/filter.service';

describe('ProductListPageComponent', () => {
  let component: ProductListPageComponent;
  let fixture: ComponentFixture<ProductListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductDetailPageComponent,
        SearchListPageComponent,
        ProductListPageComponent,
        ProductListComponent,
        ProductOnListComponent,
        CategoryNavigationComponent,
        ProductListFilterManagerComponent,
        BuyButtonComponent,
        ProductReviewsComponent,
        ProductPhotoComponent
      ],
      imports: [
        HttpClientModule,
        FormsModule,
        SharedModule,
        MatProgressSpinnerModule,
        SwiperModule,
        NgbRatingModule.forRoot(),
        FontAwesomeModule,
        RouterTestingModule
      ],
      providers: [
        ProductRepository,
        ProductClient,
        ProductFactory,
        PhotoFactory,
        CategoryClient,
        CategoryRepository,
        CategoryFactory,
        AttributeFactory,
        AttributeOptionFactory,
        FilterFactory,
        ReviewRepository,
        ReviewFactory,
        OrderItemFactory,
        OrderService,
        OrderProvider,
        FilterService,
        {
          provide: PRODUCT_MODULE_GLOBAL_CONFIG,
          useValue: {
            checkoutUrl: '/checkout'
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
