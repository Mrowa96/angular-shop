import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Product} from '../../models/product';
import {ProductRepository} from '../../repositories/product.repository';
import {Breadcrumb} from '../../../shared/models/breadcrumb';

@Component({
  templateUrl: './search-list-page.component.html',
  styleUrls: ['./search-list-page.component.scss']
})
export class SearchListPageComponent implements OnInit {
  public productCollection: Array<Product> | undefined;
  private _phrase: string;

  public constructor(private route: ActivatedRoute, private productRepository: ProductRepository) {
  }

  public ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this._phrase = params.get('phrase');

      this.productRepository.getAllByPhrase(this._phrase).subscribe((productCollection: Array<Product>) => {
        this.productCollection = productCollection;
      });
    });
  }

  public get breadcrumbsItems(): Array<Breadcrumb> {
    return [
      new Breadcrumb('Home', ['/']),
      new Breadcrumb('Search'),
      new Breadcrumb(this._phrase),
    ];
  }

  public get phrase(): string {
    return this._phrase;
  }
}
