import {CategoryFactory} from './category.factory';
import {Category} from '../models/category';
import {CategoryResponseInterface} from '../interfaces/category-response.interface';
import {AttributeFactory} from './attribute.factory';
import {AttributeOptionFactory} from './attribute-option.factory';

describe('CategoryFactory', () => {
  let factory: CategoryFactory;

  beforeEach(() => {
    factory = new CategoryFactory(new AttributeFactory(new AttributeOptionFactory()));
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Category object', function () {
    const data = <CategoryResponseInterface>{
      id: 1,
      name: 'Test category',
      attributes: []
    };
    const category = factory.create(data);

    expect(category instanceof Category).toBeTruthy();
    expect(category.name).toBe(data.name);
  });
});
