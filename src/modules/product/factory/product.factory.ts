import {Product} from '../models/product';
import {ProductResponseInterface} from '../interfaces/product-response.interface';
import {Injectable} from '@angular/core';
import {PhotoFactory} from './photo.factory';
import {CategoryFactory} from './category.factory';
import {ReviewFactory} from './review.factory';
import {AttributeFactory} from './attribute.factory';

@Injectable()
export class ProductFactory {
  public constructor(
    private photoFactory: PhotoFactory,
    private categoryFactory: CategoryFactory,
    private reviewFactory: ReviewFactory,
    private attributeFactory: AttributeFactory) {
  }

  public create(data: ProductResponseInterface): Product {
    return new Product(
      data.id,
      data.name,
      data.price,
      this.categoryFactory.create(data.category),
      this.photoFactory.createCollection(data.photos),
      this.reviewFactory.createCollection(data.reviews),
      this.attributeFactory.createCollection(data.attributes)
    );
  }

  public createCollection(data: ProductResponseInterface[]): Product[] {
    const productCollection: Array<Product> = [];

    for (const productData of data) {
      productCollection.push(this.create(productData));
    }

    return productCollection;
  }
}
