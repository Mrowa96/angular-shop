import {FilterFactory} from './filter.factory';

describe('FilterFactory', () => {
  let factory: FilterFactory;

  beforeEach(() => {
    factory = new FilterFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Filter object', function () {
    const filter = factory.create(1, 2);

    expect(filter.attributeId).toEqual(1);
    expect(filter.attributeOptionId).toEqual(2);
  });
});
