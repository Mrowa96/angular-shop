import {ReviewFactory} from './review.factory';
import {Review} from '../models/review';
import {ReviewResponseInterface} from '../interfaces/review-response.interface';

describe('ReviewFactory', () => {
  let factory: ReviewFactory;

  beforeEach(() => {
    factory = new ReviewFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Review object', function () {
    const data = <ReviewResponseInterface>{
      id: 2000,
      rating: 5,
      authorName: 'Jan Kowalski',
      date: '2017-10-10 18:02:34',
      content: 'Great console! Best on the market.'
    };
    const review = factory.create(data);

    expect(review instanceof Review).toBeTruthy();
    expect(review.rating).toBe(data.rating);
  });
});
