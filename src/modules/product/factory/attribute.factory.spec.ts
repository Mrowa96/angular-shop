import {Category} from '../models/category';
import {CategoryResponseInterface} from '../interfaces/category-response.interface';
import {AttributeFactory} from './attribute.factory';
import {AttributeOptionFactory} from './attribute-option.factory';
import {AttributeResponseInterface} from '../interfaces/attribute-response.interface';
import {Attribute} from '../models/attribute';

describe('AttributeFactory', () => {
  let factory: AttributeFactory;

  beforeEach(() => {
    factory = new AttributeFactory(new AttributeOptionFactory());
  });

  afterEach(() => {
    factory = null;
  });

  it('should return Attribute object',  () => {
    const data = <AttributeResponseInterface>{
      id: 1,
      name: 'Test category',
      filterable: true,
      options: [
        {
          id: 1,
          name: 'Test'
        }
      ]
    };
    const attribute = factory.create(data);

    expect(attribute instanceof Attribute).toBeTruthy();
    expect(attribute.name).toBe(data.name);
  });
});
