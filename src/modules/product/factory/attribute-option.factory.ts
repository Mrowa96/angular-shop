import {AttributeOption} from '../models/attribute-option';
import {AttributeOptionResponseInterface} from '../interfaces/attribute-option-response.interface';
import {Injectable} from '@angular/core';

@Injectable()
export class AttributeOptionFactory {
  public create(data: AttributeOptionResponseInterface): AttributeOption {
    return new AttributeOption(data.id, data.name);
  }

  public createCollection(data: AttributeOptionResponseInterface[]): Array<AttributeOption> {
    const attributeOptionCollection: Array<AttributeOption> = [];

    for (const attributeOptionData of data) {
      attributeOptionCollection.push(this.create(attributeOptionData));
    }

    return attributeOptionCollection;
  }
}
