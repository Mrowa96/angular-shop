import {Attribute} from '../models/attribute';
import {AttributeResponseInterface} from '../interfaces/attribute-response.interface';
import {Injectable} from '@angular/core';
import {AttributeOptionFactory} from './attribute-option.factory';

@Injectable()
export class AttributeFactory {
  public constructor(private attributeOptionFactory: AttributeOptionFactory) {
  }

  public create(data: AttributeResponseInterface): Attribute {
    return new Attribute(
      data.id,
      data.name,
      data.filterable,
      this.attributeOptionFactory.createCollection(data.options)
    );
  }

  public createCollection(data: AttributeResponseInterface[]): Array<Attribute> {
    const attributeCollection: Array<Attribute> = [];

    for (const attributeData of data) {
      attributeCollection.push(this.create(attributeData));
    }

    return attributeCollection;
  }
}
