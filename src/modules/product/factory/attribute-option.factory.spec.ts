import {AttributeOptionFactory} from './attribute-option.factory';
import {AttributeOptionResponseInterface} from '../interfaces/attribute-option-response.interface';

describe('AttributeOptionFactory', () => {
  let factory: AttributeOptionFactory;

  beforeEach(() => {
    factory = new AttributeOptionFactory();
  });

  afterEach(() => {
    factory = null;
  });

  it('should return AttributeOption object', () => {
    const data = <AttributeOptionResponseInterface>{
      id: 1,
      name: 'Test'
    };

    const attributeOption = factory.create(data);

    expect(attributeOption.id).toEqual(data.id);
    expect(attributeOption.name).toEqual(data.name);
  });

  it('should return array of AttributeOption objects', () => {
    const data = [
      <AttributeOptionResponseInterface>{
        id: 1,
        name: 'Test'
      },
      <AttributeOptionResponseInterface>{
        id: 2,
        name: 'Test 2'
      },
    ];

    const attributeOptionCollection = factory.createCollection(data);

    expect(attributeOptionCollection.length).toBe(data.length);
  });
});
