import {Photo} from '../models/photo';
import {PhotoResponseInterface} from '../interfaces/photo-response.interface';
import {Injectable} from '@angular/core';

@Injectable()
export class PhotoFactory {
  public create(data: PhotoResponseInterface): Photo {
    return new Photo(data.id, data.url, data.main);
  }

  public createCollection(data: PhotoResponseInterface[]): Photo[] {
    const collection: Array<Photo> = [];

    for (const photoData of data) {
      collection.push(this.create(photoData));
    }

    return collection;
  }
}
