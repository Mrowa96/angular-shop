import {Injectable} from '@angular/core';
import {Filter} from '../models/filter';

@Injectable()
export class FilterFactory {
  public create(attributeId: number, attributeOptionId: number): Filter {
    return new Filter(attributeId, attributeOptionId);
  }

  public createCollection(data: string): Filter[] {
    const filterCollection: Array<Filter> = [];

    for (const filterData of data.split(',')) {
      const filterDataArray = filterData.split('-');

      if (filterDataArray.length === 2) {
        filterCollection.push(this.create(
          parseInt(filterDataArray[0], 10),
          parseInt(filterDataArray[1], 10)
        ));
      }
    }

    return filterCollection;
  }
}
