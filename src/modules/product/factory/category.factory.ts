import {CategoryResponseInterface} from '../interfaces/category-response.interface';
import {Category} from '../models/category';
import {Injectable} from '@angular/core';
import {AttributeFactory} from './attribute.factory';

@Injectable()
export class CategoryFactory {
  public constructor(private attributeFactory: AttributeFactory) {
  }

  public create(data: CategoryResponseInterface): Category {
    let attributes = [];

    if (data.attributes) {
      attributes = this.attributeFactory.createCollection(data.attributes);
    }

    return new Category(data.id, data.name, attributes);
  }

  public createCollection(data: CategoryResponseInterface[]): Category[] {
    const categoryCollection: Array<Category> = [];

    for (const categoryData of data) {
      categoryCollection.push(this.create(categoryData));
    }

    return categoryCollection;
  }
}
