import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {CheckoutPageComponent} from './pages/checkout-page/checkout-page.component';
import {CheckoutRoutingModule} from './checkout-routing.module';
import {OrderService} from './services/order.service';
import {OrderItemFactory} from './factory/order-item.factory';
import {AddressManagerComponent} from './components/address-manager/address-manager.component';
import {OrderItemListComponent} from './components/order-item-list/order-item-list.component';
import {TransportManagerComponent} from './components/transport-manager/transport-manager.component';
import {PaymentManagerComponent} from './components/payment-manager/payment-manager.component';
import {ProductModule} from '../product/product.module';
import {FormsModule} from '@angular/forms';
import {TransportFactory} from './factory/transport.factory';
import {PaymentFactory} from './factory/payment.factory';
import {TransportClient} from './client/transport.client';
import {PaymentClient} from './client/payment.client';
import {TransportRepository} from './repositories/transport.repository';
import {PaymentRepository} from './repositories/payment.repository';
import {HttpClientModule} from '@angular/common/http';
import {CheckoutSummaryPageComponent} from './pages/checkout-summary-page/checkout-summary-page.component';
import {CheckoutSummaryGuard} from './guards/checkout-summary.guard';
import {CheckoutInfoComponent} from './components/checkout-info/checkout-info.component';
import {CheckoutStatusPageComponent} from './pages/checkout-status-page/checkout-status-page.component';
import {CheckoutStatusGuard} from './guards/checkout-status.guard';
import {OrderProvider} from './provider/order.provider';
import {CheckoutSectionComponent} from './components/checkout-section/checkout-section.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {OrderFactory} from './factory/order.factory';
import {LocalOrderService} from './services/local-order.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ProductModule,
    HttpClientModule,
    FontAwesomeModule,
    CheckoutRoutingModule
  ],
  exports: [
    CheckoutInfoComponent,
  ],
  entryComponents: [
    OrderItemListComponent,
    AddressManagerComponent,
    PaymentManagerComponent,
    TransportManagerComponent
  ],
  declarations: [
    CheckoutPageComponent,
    CheckoutSummaryPageComponent,
    CheckoutStatusPageComponent,
    CheckoutSectionComponent,
    AddressManagerComponent,
    OrderItemListComponent,
    TransportManagerComponent,
    PaymentManagerComponent,
    CheckoutInfoComponent
  ],
  providers: [
    OrderItemFactory,
    TransportFactory,
    PaymentFactory,
    TransportClient,
    PaymentClient,
    TransportRepository,
    PaymentRepository,
    OrderService,
    OrderProvider,
    CheckoutSummaryGuard,
    CheckoutStatusGuard,
    OrderFactory,
    LocalOrderService
  ]
})
export class CheckoutModule {
}
