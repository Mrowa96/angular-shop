import {Order} from '../models/order';
import {EventEmitter} from '@angular/core';

export interface CheckoutSectionComponentInterface {
  order: Order;
  state: boolean | undefined;
  visibility?: EventEmitter<boolean>;
  shouldBeVisible(): boolean;
}
