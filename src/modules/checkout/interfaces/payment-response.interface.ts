export interface PaymentResponseInterface {
  id: number;
  name: string;
  cost: number;
}
