import {Product} from '../../product/models/product';

export class OrderItem {
  public constructor(private _id: string, private _quantity: number, private _cost: number, private _product: Product) {
  }

  public get id(): string {
    return this._id;
  }

  public incrementQuantity(): void {
    this._quantity++;
  }

  // TODO Temp
  public set quantity(value: number) {
    this._quantity = value;
  }

  public get quantity(): number {
    return this._quantity;
  }

  public get cost(): number {
    return this._cost;
  }

  public get product(): Product {
    return this._product;
  }

  public get totalCost(): number {
    return this.quantity * this.cost;
  }
}
