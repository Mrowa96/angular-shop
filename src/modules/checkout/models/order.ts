import {OrderItem} from './order-item';
import {Address} from '../../client/models/address';
import {Payment} from './payment';
import {Transport} from './transport';
import {OrderState} from '../value/order-state.value';

export class Order {
  public constructor(
    private _id: string,
    private _items: Array<OrderItem> = [],
    private _address?: Address,
    private _transport?: Transport,
    private _payment?: Payment,
    private _state: OrderState = new OrderState()) {
  }

  public get id(): string {
    return this._id;
  }

  public addItem(orderItem: OrderItem): void {
    this._items.push(orderItem);
  }

  public get address(): Address {
    return this._address;
  }

  public set address(value: Address) {
    this._address = value;
  }

  public get transport(): Transport {
    return this._transport;
  }

  public set transport(value: Transport) {
    this._transport = value;
  }

  public get payment(): Payment {
    return this._payment;
  }

  public set payment(value: Payment) {
    this._payment = value;
  }

  public get state(): OrderState {
    return this._state;
  }

  public get items(): Array<OrderItem> {
    return this._items;
  }

  public get totalCost(): number {
    let totalCost = 0;

    for (const orderItem of this.items) {
      totalCost += orderItem.cost * orderItem.quantity;
    }

    if (this.payment) {
      totalCost += this.payment.cost;
    }

    if (this.transport) {
      totalCost += this.transport.cost;
    }

    return totalCost;
  }

  public get totalQuantity(): number {
    let totalQuantity = 0;

    for (const orderItem of this.items) {
      totalQuantity += orderItem.quantity;
    }

    return totalQuantity;
  }
}
