import {AddressDTO} from '../../client/dto/address.dto';
import {OrderState} from '../value/order-state.value';
import {OrderItemMinimalDTO} from './order-item-minimal.dto';

export class OrderMinimalDTO {
  public constructor(
    public id: string,
    public items: Array<OrderItemMinimalDTO> = [],
    public address?: AddressDTO,
    public transportId?: number,
    public paymentId?: number,
    public state?: OrderState
  ) {
  }
}
