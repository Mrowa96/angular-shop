export class OrderItemMinimalDTO {
  public constructor(
    public id?: string,
    public productId?: number,
    public quantity?: number,
    public cost?: number) {
  }
}
