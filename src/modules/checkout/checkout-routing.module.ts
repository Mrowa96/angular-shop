import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CheckoutPageComponent} from './pages/checkout-page/checkout-page.component';
import {CheckoutSummaryPageComponent} from './pages/checkout-summary-page/checkout-summary-page.component';
import {CheckoutSummaryGuard} from './guards/checkout-summary.guard';
import {CheckoutStatusPageComponent} from './pages/checkout-status-page/checkout-status-page.component';
import {CheckoutStatusGuard} from './guards/checkout-status.guard';

const routes: Routes = [
  {
    path: 'checkout',
    component: CheckoutPageComponent
  },
  {
    path: 'checkout/summary',
    component: CheckoutSummaryPageComponent,
    canActivate: [CheckoutSummaryGuard]
  },
  {
    path: 'checkout/status',
    component: CheckoutStatusPageComponent,
    canActivate: [CheckoutStatusGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CheckoutRoutingModule {
}
