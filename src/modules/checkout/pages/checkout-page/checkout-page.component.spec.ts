import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CheckoutPageComponent} from './checkout-page.component';
import {OrderItemFactory} from '../../factory/order-item.factory';
import {OrderService} from '../../services/order.service';
import {RouterTestingModule} from '@angular/router/testing';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {ClientModule} from '../../../client/client.module';
import {ProductModule} from '../../../product/product.module';
import {PaymentRepository} from '../../repositories/payment.repository';
import {PaymentClient} from '../../client/payment.client';
import {PaymentFactory} from '../../factory/payment.factory';
import {TransportFactory} from '../../factory/transport.factory';
import {TransportRepository} from '../../repositories/transport.repository';
import {TransportClient} from '../../client/transport.client';
import {HttpClientModule} from '@angular/common/http';
import {OrderItemListComponent} from '../../components/order-item-list/order-item-list.component';
import {AddressManagerComponent} from '../../components/address-manager/address-manager.component';
import {TransportManagerComponent} from '../../components/transport-manager/transport-manager.component';
import {PaymentManagerComponent} from '../../components/payment-manager/payment-manager.component';
import {OrderProvider} from '../../provider/order.provider';
import {CheckoutSectionComponent} from '../../components/checkout-section/checkout-section.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {Observable, Observer} from 'rxjs';
import {Order} from '../../models/order';
import {OrderState} from '../../value/order-state.value';

describe('CheckoutPageComponent', () => {
  let component: CheckoutPageComponent;
  let fixture: ComponentFixture<CheckoutPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CheckoutPageComponent,
        OrderItemListComponent,
        AddressManagerComponent,
        TransportManagerComponent,
        PaymentManagerComponent,
        CheckoutSectionComponent
      ],
      imports: [
        HttpClientModule,
        FormsModule,
        SharedModule,
        ClientModule,
        ProductModule,
        FontAwesomeModule,
        RouterTestingModule,
      ],
      providers: [
        OrderItemFactory,
        PaymentRepository,
        PaymentClient,
        PaymentFactory,
        TransportRepository,
        TransportClient,
        TransportFactory,
        OrderService,
        {
          provide: OrderProvider,
          useValue: {
            orderObservable: Observable.create((observer: Observer<Order>) => {
              observer.next(
                new Order('1', [], undefined, undefined, undefined, new OrderState(true))
              );
            })
          }
        }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
