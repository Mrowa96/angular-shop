import {Component, OnDestroy, OnInit} from '@angular/core';
import {Order} from '../../models/order';
import {OrderProvider} from '../../provider/order.provider';
import {OrderService} from '../../services/order.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'checkout-status-page',
  templateUrl: './checkout-status-page.component.html',
  styleUrls: ['./checkout-status-page.component.scss']
})
export class CheckoutStatusPageComponent implements OnInit, OnDestroy {
  public isSaved: boolean = false;
  private orderSubscription: Subscription | undefined;

  public constructor(private orderProvider: OrderProvider, private orderService: OrderService) {
  }

  public ngOnInit(): void {
    this.orderSubscription = this.orderProvider.orderObservable.subscribe((order: Order) => {
      if (order.state.saved !== undefined) {
        this.isSaved = order.state.saved;

        this.orderService.clearOrder();
      }
    });
  }

  public ngOnDestroy(): void {
    if (this.orderSubscription) {
      this.orderSubscription.unsubscribe();
    }
  }
}
