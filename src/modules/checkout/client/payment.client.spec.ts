import {TestBed, getTestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {PaymentResponseInterface} from '../interfaces/payment-response.interface';
import {PaymentClient} from './payment.client';

describe('PaymentClient', () => {
  let injector: TestBed;
  let service: PaymentClient;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PaymentClient]
    });
    injector = getTestBed();
    service = injector.get(PaymentClient);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('#getOne', () => {
    it('should return an Observable<PaymentResponseInterface>', () => {
      const dummyResponse = <PaymentResponseInterface>{
        id: 1,
        name: 'Test',
        cost: 9.99
      };

      service.getOne(1).subscribe(data => {
        expect(data).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(`${PaymentClient.URL}/${dummyResponse.id}`);

      expect(req.request.method).toBe('GET');

      req.flush(dummyResponse);
    });
  });

  describe('#getAll', () => {
    it('should return an Observable<PaymentResponseInterface[]>', () => {
      const dummyResponse = [
        <PaymentResponseInterface>{
          id: 1,
          name: 'Test',
          cost: 9.98
        },
        <PaymentResponseInterface>{
          id: 2,
          name: 'Test 2',
          cost: 0.99
        }
      ];

      service.getAll().subscribe(data => {
        expect(data.length).toBe(2);
        expect(data).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(PaymentClient.URL);

      expect(req.request.method).toBe('GET');

      req.flush(dummyResponse);
    });
  });
});
