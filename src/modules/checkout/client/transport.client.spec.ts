import {TestBed, getTestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TransportResponseInterface} from '../interfaces/transport-response.interface';
import {TransportClient} from './transport.client';

describe('Trapayment.client.spec.tsnsportClient', () => {
  let injector: TestBed;
  let service: TransportClient;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TransportClient]
    });
    injector = getTestBed();
    service = injector.get(TransportClient);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('#getOne', () => {
    it('should return an Observable<TransportResponseInterface>', () => {
      const dummyResponse = <TransportResponseInterface>{
        id: 1,
        name: 'Test',
        cost: 9.99
      };

      service.getOne(1).subscribe(data => {
        expect(data).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(`${TransportClient.URL}/${dummyResponse.id}`);

      expect(req.request.method).toBe('GET');

      req.flush(dummyResponse);
    });
  });

  describe('#getAll', () => {
    it('should return an Observable<TransportResponseInterface[]>', () => {
      const dummyResponse = [
        <TransportResponseInterface>{
          id: 1,
          name: 'Test',
          cost: 9.98
        },
        <TransportResponseInterface>{
          id: 2,
          name: 'Test 2',
          cost: 0.99
        }
      ];

      service.getAll().subscribe(data => {
        expect(data.length).toBe(2);
        expect(data).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(TransportClient.URL);

      expect(req.request.method).toBe('GET');

      req.flush(dummyResponse);
    });
  });
});
