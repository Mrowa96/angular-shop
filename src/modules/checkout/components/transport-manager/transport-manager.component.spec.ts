import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TransportManagerComponent} from './transport-manager.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TransportRepository} from '../../repositories/transport.repository';
import {TransportClient} from '../../client/transport.client';
import {TransportFactory} from '../../factory/transport.factory';
import {OrderService} from '../../services/order.service';

describe('TransportManagerComponent', () => {
  let component: TransportManagerComponent;
  let fixture: ComponentFixture<TransportManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransportManagerComponent],
      imports: [
        FormsModule,
        HttpClientModule
      ],
      providers: [
        TransportRepository,
        TransportClient,
        TransportFactory,
        {
          provide: OrderService,
          useValue: {
            updateOrder: () => {}
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
