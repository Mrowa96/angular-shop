import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order} from '../../models/order';
import {Transport} from '../../models/transport';
import {TransportRepository} from '../../repositories/transport.repository';
import {CheckoutSectionComponentInterface} from '../../interfaces/checkout-section-component.interface';
import {OrderService} from '../../services/order.service';

@Component({
  selector: 'checkout-transport-manager',
  templateUrl: './transport-manager.component.html',
  styleUrls: ['./transport-manager.component.scss']
})
export class TransportManagerComponent implements OnInit, CheckoutSectionComponentInterface {
  @Input()
  public order: Order;

  @Output()
  public visibility = new EventEmitter<boolean>();

  public transportCollection: Array<Transport> = [];

  public constructor(private transportRepository: TransportRepository, private orderService: OrderService) {}

  public onButtonClick(): void {
    this.order.state.transportProcessed = true;
    this.orderService.updateOrder(this.order);
    this.visibility.emit(false);
  }

  public ngOnInit(): void {
    this.transportRepository.getAll().subscribe((transportCollection: Array<Transport>) => {
      this.transportCollection = transportCollection;
    });
  }

  public shouldBeVisible(): boolean {
    return (this.state === undefined || this.state === false) && this.order.state.paymentProcessed === true;
  }

  public get state(): boolean | undefined {
    return this.order.state.transportProcessed;
  }
}
