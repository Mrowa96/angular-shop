import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order} from '../../models/order';
import {PaymentRepository} from '../../repositories/payment.repository';
import {Payment} from '../../models/payment';
import {CheckoutSectionComponentInterface} from '../../interfaces/checkout-section-component.interface';
import {OrderService} from '../../services/order.service';

@Component({
  selector: 'checkout-payment-manager',
  templateUrl: './payment-manager.component.html',
  styleUrls: ['./payment-manager.component.scss']
})
export class PaymentManagerComponent implements OnInit, CheckoutSectionComponentInterface {
  @Input()
  public order: Order;

  @Output()
  public visibility = new EventEmitter<boolean>();

  public paymentCollection: Array<Payment> = [];

  public constructor(private paymentRepository: PaymentRepository, private orderService: OrderService) {}

  public onButtonClick(): void {
    this.order.state.paymentProcessed = true;
    this.orderService.updateOrder(this.order);
    this.visibility.emit(false);
  }

  public ngOnInit(): void {
    this.paymentRepository.getAll().subscribe((paymentCollection: Array<Payment>) => {
      this.paymentCollection = paymentCollection;
    });
  }

  public shouldBeVisible(): boolean {
    return (this.state === undefined || this.state === false) && this.order.state.addressProcessed === true;
  }

  public get state(): boolean | undefined {
    return this.order.state.paymentProcessed;
  }
}
