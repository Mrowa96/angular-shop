import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CheckoutSectionComponent} from './checkout-section.component';
import {SharedModule} from '../../../shared/shared.module';
import {AddressManagerComponent} from '../address-manager/address-manager.component';
import {PaymentManagerComponent} from '../payment-manager/payment-manager.component';
import {TransportManagerComponent} from '../transport-manager/transport-manager.component';
import {OrderItemListComponent} from '../order-item-list/order-item-list.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ProductModule} from '../../../product/product.module';
import {ClientModule} from '../../../client/client.module';

describe('CheckoutSectionComponent', () => {
  let component: CheckoutSectionComponent;
  let fixture: ComponentFixture<CheckoutSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CheckoutSectionComponent,
        OrderItemListComponent,
        AddressManagerComponent,
        PaymentManagerComponent,
        TransportManagerComponent
      ],
      imports: [
        SharedModule,
        FormsModule,
        HttpClientModule,
        ProductModule,
        ClientModule
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
