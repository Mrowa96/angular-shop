import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Order} from '../../models/order';
import {AddressDTO} from '../../../client/dto/address.dto';
import {AddressFactory} from '../../../client/factory/address.factory';
import {CheckoutSectionComponentInterface} from '../../interfaces/checkout-section-component.interface';
import {ClientProvider} from '../../../client/provider/client.provider';
import {Client} from '../../../client/models/client';
import {NgForm} from '@angular/forms';
import {OrderService} from '../../services/order.service';

@Component({
  selector: 'checkout-address-manager',
  templateUrl: './address-manager.component.html',
  styleUrls: ['./address-manager.component.scss']
})
export class AddressManagerComponent implements OnInit, CheckoutSectionComponentInterface {
  @Input()
  public order: Order;

  @Output()
  public visibility = new EventEmitter<boolean>();

  @ViewChild('addressForm')
  private addressFormRef: NgForm;

  public address: AddressDTO;
  public client: Client | undefined;
  public showEditMessage: boolean = false;

  public constructor(
    private addressFactory: AddressFactory,
    private clientProvider: ClientProvider,
    private orderService: OrderService) {
  }

  public ngOnInit(): void {
    if (this.order && this.order.address) {
      this.newAddressFromOrder();
    } else {
      this.newAddress();
    }

    this.clientProvider.clientObservable.subscribe((client: Client) => {
      this.client = client;
    });
  }

  public setAddress(): void {
    this.order.address = this.addressFactory.create(this.address);
  }

  private newAddress(): void {
    this.address = new AddressDTO();
  }

  private newAddressFromOrder(): void {
    this.address = new AddressDTO(
      undefined,
      this.order.address.name,
      this.order.address.surname,
      this.order.address.street,
      this.order.address.buildingNumber,
      this.order.address.city,
      this.order.address.zip
    );
  }

  private populateWithClientAddress(): void {
    this.address = new AddressDTO(
      undefined,
      this.client.address.name,
      this.client.address.surname,
      this.client.address.street,
      this.client.address.buildingNumber,
      this.client.address.city,
      this.client.address.zip
    );

    setTimeout(() => {
      this.onFormChange();
    }, 1);
  }

  public onButtonClick(): void {
    this.setAddress();

    this.visibility.emit(false);
    this.showEditMessage = false;
    this.order.state.addressProcessed = true;
    this.orderService.updateOrder(this.order);
  }

  public onFormChange(): void {
    if (this.addressFormRef.valid === false) {
      this.order.state.addressProcessed = false;
    } else {
      this.order.state.addressProcessed = undefined;
    }

    if (this.order.address) {
      this.showEditMessage = true;
    }
  }

  public shouldBeVisible(): boolean {
    return (this.state === undefined || this.state === false) && this.order.state.orderItemsProcessed === true;
  }

  public get state(): boolean | undefined {
    return this.order.state.addressProcessed;
  }
}
