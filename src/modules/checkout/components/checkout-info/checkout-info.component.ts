import {Component, OnInit} from '@angular/core';
import {Order} from '../../models/order';
import {OrderProvider} from '../../provider/order.provider';

@Component({
  selector: 'checkout-info',
  templateUrl: './checkout-info.component.html',
  styleUrls: ['./checkout-info.component.scss']
})
export class CheckoutInfoComponent implements OnInit {
  public order: Order;

  public constructor(private orderProvider: OrderProvider) {}

  public ngOnInit(): void {
    this.orderProvider.orderObservable.subscribe((order: Order) => {
      this.order = order;
    });
  }
}
