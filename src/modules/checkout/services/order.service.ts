import {Injectable} from '@angular/core';
import {OrderServiceInterface} from '../../product/interfaces/order.service.interface';
import {OrderItemDTO} from '../dto/order-item.dto';
import {Order} from '../models/order';
import {OrderItemFactory} from '../factory/order-item.factory';
import {Product} from '../../product/models/product';
import {OrderItem} from '../models/order-item';
import {OrderProvider} from '../provider/order.provider';

@Injectable()
export class OrderService implements OrderServiceInterface {
  private order: Order | undefined;

  public constructor(private orderItemFactory: OrderItemFactory, private orderProvider: OrderProvider) {
    this.orderProvider.orderObservable.subscribe((order: Order) => {
      this.order = order;
    });
  }

  public addOrderItem(orderItemDTO: OrderItemDTO): void {
    let orderItem = this.getOrderItemByProduct(orderItemDTO.product);

    if (orderItem) {
      orderItem.incrementQuantity();
    } else {
      orderItem = this.orderItemFactory.create(orderItemDTO);

      this.order.addItem(orderItem);
    }

    this.orderProvider.refresh(this.order);
  }

  public removeOrderItem(orderItem: OrderItem): void {
    const index = this.order.items.indexOf(orderItem);

    if (index > -1) {
      this.order.items.splice(index, 1);
    }

    this.orderProvider.refresh(this.order);
  }

  public saveOrder(): void {
    this.order.state.saved = true;

    this.orderProvider.refresh(this.order);
  }

  public updateOrder(order: Order): void {
    this.orderProvider.refresh(order);
  }

  public clearOrder(): void {
    this.orderProvider.refresh();
  }

  public isReadyForSummary(): boolean {
    return !!(this.order.state.orderItemsProcessed && this.order.state.addressProcessed &&
      this.order.state.paymentProcessed && this.order.state.transportProcessed);
  }

  private getOrderItemByProduct(product: Product): OrderItem | undefined {
    return this.order.items.find((orderItem: OrderItem) => {
      return orderItem.product.id === product.id;
    });
  }
}
