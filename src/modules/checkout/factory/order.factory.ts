import {Order} from '../models/order';
import {OrderDTO} from '../dto/order.dto';
import {Injectable} from '@angular/core';
import {OrderState} from '../value/order-state.value';
import {v4 as uuid} from 'uuid';

@Injectable()
export class OrderFactory {
  public create(orderDTO: OrderDTO): Order {
    return new Order(orderDTO.id, orderDTO.items, orderDTO.address, orderDTO.transport, orderDTO.payment, orderDTO.state);
  }

  public createEmpty(): Order {
    return new Order(uuid(), [], undefined, undefined, undefined, new OrderState(true));
  }
}
