import {Injectable} from '@angular/core';
import {PaymentResponseInterface} from '../interfaces/payment-response.interface';
import {Payment} from '../models/payment';

@Injectable()
export class PaymentFactory {
  public create(data: PaymentResponseInterface): Payment {
    return new Payment(data.id, data.name, data.cost);
  }

  public createCollection(data: PaymentResponseInterface[]): Payment[] {
    const paymentCollection: Array<Payment> = [];

    for (const paymentData of data) {
      paymentCollection.push(this.create(paymentData));
    }

    return paymentCollection;
  }
}
