export class Theme {
  public primaryColor: string;
  public primaryColor1: string;
  public primaryColor2: string;

  public constructor(private _key: string, private _name: string) {
  }

  public get key(): string {
    return this._key;
  }

  public get name(): string {
    return this._name;
  }
}
