import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {APP_BASE_HREF} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {NotFoundPageComponent} from './pages/not-found-page/not-found-page.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {HeaderComponent} from './components/header/header.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {FooterComponent} from './components/footer/footer.component';
import {CheckoutModule} from '../checkout/checkout.module';
import {MatSidenavModule} from '@angular/material';
import {ProductModule} from '../product/product.module';
import {SwiperModule} from 'ngx-swiper-wrapper';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {ClientModule} from '../client/client.module';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderItemFactory} from '../checkout/factory/order-item.factory';
import {OrderService} from '../checkout/services/order.service';
import {OrderProvider} from '../checkout/provider/order.provider';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ThemeService} from './services/theme.service';
import {ThemeFactory} from './factory/theme.factory';
import {ThemeManagerComponent} from './components/theme-manager/theme-manager.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        SidebarComponent,
        FooterComponent,
        NavigationComponent,
        HomePageComponent,
        NotFoundPageComponent,
        ThemeManagerComponent
      ],
      imports: [
        BrowserAnimationsModule,
        SwiperModule,
        HttpClientModule,
        NgbDropdownModule.forRoot(),
        SharedModule,
        ProductModule,
        ClientModule,
        CheckoutModule,
        MatSidenavModule,
        FontAwesomeModule,
        AppRoutingModule,
        RouterTestingModule
      ],
      providers: [
        OrderItemFactory,
        OrderProvider,
        OrderService,
        ThemeService,
        ThemeFactory,
        {
          provide: APP_BASE_HREF, useValue: '/'
        }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    expect(app).toBeTruthy();
  }));

  it(`should have app name`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    expect(app.appName).toEqual('Shop');
  }));
});
