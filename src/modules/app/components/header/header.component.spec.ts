import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HeaderComponent} from './header.component';
import {NavigationComponent} from '../navigation/navigation.component';
import {SharedModule} from '../../../shared/shared.module';
import {ProductModule} from '../../../product/product.module';
import {ClientModule} from '../../../client/client.module';
import {CheckoutModule} from '../../../checkout/checkout.module';
import {MatSidenavModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {OrderItemFactory} from '../../../checkout/factory/order-item.factory';
import {OrderService} from '../../../checkout/services/order.service';
import {OrderProvider} from '../../../checkout/provider/order.provider';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ThemeService} from '../../services/theme.service';
import {ThemeFactory} from '../../factory/theme.factory';
import {ThemeManagerComponent} from '../theme-manager/theme-manager.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let native: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent,
        NavigationComponent,
        ThemeManagerComponent
      ],
      imports: [
        SharedModule,
        ProductModule,
        ClientModule,
        CheckoutModule,
        MatSidenavModule,
        NgbDropdownModule.forRoot(),
        FontAwesomeModule,
        RouterTestingModule
      ],
      providers: [
        OrderItemFactory,
        OrderProvider,
        OrderService,
        ThemeService,
        ThemeFactory
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have home page link', () => {
    expect(native.querySelector('.app-name').getAttribute('href')).toEqual('/');
  });
});
