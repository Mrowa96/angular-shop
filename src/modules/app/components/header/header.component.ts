import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatSidenav} from '@angular/material';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input()
  public appName: string;

  @Input()
  public sidenav: MatSidenav;

  @Output()
  public sidenavToggle = new EventEmitter<boolean>();

  public onSidenavButtonClick(): void {
    this.sidenav.open();
    this.sidenavToggle.emit(true);
  }
}
