import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ThemeService} from '../../services/theme.service';
import {DropdownItem} from '../../../shared/models/dropdown-item';
import {Theme} from '../../models/theme';

@Component({
  selector: 'app-theme-manager',
  templateUrl: './theme-manager.component.html',
  styleUrls: ['./theme-manager.component.scss']
})
export class ThemeManagerComponent {
  @Input()
  public mobileVersion: boolean = false;

  @Output()
  public themeSelect = new EventEmitter();

  public constructor(private themeService: ThemeService) {
  }

  public toggleTheme(item: DropdownItem): void {
    this.themeService.changeThemeSchema(item.text);

    this.themeSelect.emit();
  }

  public get dropdownThemeItems(): Array<DropdownItem> {
    return this.themeService.themes.map((theme: Theme) => {
      return new DropdownItem(theme.name, ['#']);
    });
  }
}
