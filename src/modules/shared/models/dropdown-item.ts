export class DropdownItem {
  public constructor(
    public text: string,
    public route: Array<any> | null = null,
    public isSpecial: boolean = false) {
  }
}
