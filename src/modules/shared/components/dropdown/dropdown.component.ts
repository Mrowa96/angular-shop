import {Component, EventEmitter, Input, Output} from '@angular/core';
import {DropdownItem} from '../../models/dropdown-item';
import {Router} from '@angular/router';

@Component({
  selector: 'shared-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent {
  @Input()
  public placement: string = 'bottom-right';

  @Input()
  public labelIcon: Array<string> | undefined;

  @Input()
  public labelText: string | undefined;

  @Input()
  public standardListOnMobile: boolean = false;

  @Input()
  public customContent: boolean = false;

  @Input()
  public items: Array<DropdownItem> = [];

  @Output()
  public itemClick = new EventEmitter<DropdownItem>();

  public constructor(private router: Router) {
  }

  public onItemClick(item: DropdownItem, event: MouseEvent): void {
    if (item.route) {
      if (item.route[0] === '#') {
        event.preventDefault();
      } else {
        this.router.navigate(item.route);
      }
    }

    this.itemClick.emit(item);
  }
}
