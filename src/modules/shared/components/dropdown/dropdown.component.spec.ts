import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DropdownComponent} from './dropdown.component';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {DropdownItem} from '../../models/dropdown-item';

describe('DropdownComponent', () => {
  let component: DropdownComponent;
  let fixture: ComponentFixture<DropdownComponent>;
  let native: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DropdownComponent
      ],
      imports: [
        NgbDropdownModule.forRoot(),
        FontAwesomeModule,
        RouterTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownComponent);

    component = fixture.componentInstance;
    component.items = [
      new DropdownItem('Test')
    ];

    fixture.detectChanges();

    native = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have button with "with-icon" class', () => {
    component.labelIcon = ['fas', 'user'];
    fixture.detectChanges();

    expect(native.querySelector('.dropdown-toggle').classList.contains('with-icon')).toBeTruthy();
  });

  it('should have button with "with-text" class', () => {
    component.labelText = 'Text';
    fixture.detectChanges();

    expect(native.querySelector('.dropdown-toggle').classList.contains('with-text')).toBeTruthy();
  });

  it('should have span element in button with labelText displayed', () => {
    component.labelIcon = undefined;
    component.labelText = 'Text';
    fixture.detectChanges();

    expect(native.querySelector('.dropdown-toggle span').textContent).toEqual('Text');
  });
});
