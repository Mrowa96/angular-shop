import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TitleComponent} from './title.component';

describe('TitleComponent', () => {
  let component: TitleComponent;
  let fixture: ComponentFixture<TitleComponent>;
  let native: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TitleComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TitleComponent);

    component = fixture.componentInstance;
    component.text = 'Text';

    native = fixture.debugElement.nativeElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render property in template', () => {
    expect(native.querySelector('.title').textContent).toEqual('Text');
  });

  it('should render text in h2 tag', () => {
    expect(native.querySelector('h2.title')).toBeTruthy();
  });

  it('should render text in p tag', () => {
    component.ordinaryTag = true;

    fixture.detectChanges();

    expect(native.querySelector('p.title')).toBeTruthy();
  });
});
